#!/usr/bin/python
#-*- coding: UTF-8 -*-

'''
sqlitePool 
@author: Claudio Bertuzzi thebcone@gmail.com
@license: LGPL v3 
@version: 0.01a
'''

import sqlite3
import threading

DATABASE_IS_LOCKED="database is locked"
NOT_GOOD_DATABASE ="file is encripted or is not a database"

# Definizione classi per eccezioni.
class Error(Exception):
    """ Classe base degli errori di sqlitePool.
        Da non usare dirattamente.
    """
    pass
    
class poolError(Error):
    """
        Chiamata quando qualcosa fallisce sulla gestione delle connessioni. Tipo tentativo di accedere ad una connessione non attiva
        oppure connessioni rimaste aperte alla chiusura del oggetto istanziato da sqlitePool.poolDB
    """
    pass
class prgError(Error):
    """
        Chiamata quando viene intercettato un orrore logico del programma. Un test su una situazione che
        non dovrebbe capitare, ma è capitata. In questo codice viene chiamata da getTableSchemaFromCreateStr se 
        i dati nel temporaneo che vado a creare non dovessero essere quelli attesi
    """
    pass
    
class presetError(Error):
    """ Utilizzato dalla classe poolDB per indicare un anomalia nella gestione dei Preset
    """

class dbError(Error):
    """ Classe base per gli errori sul DB:
        Non utilizzare direttamente, 
        Utilizzare le sue derivate.
    """
    def __init__(self, msg, db, cmd, par):
        self.__msg=msg
        self.__db=db
        self.__cmd=cmd
        self.__par=par
    def __str__(self):
        e_msg=self.__msg
        if self.__db<>None:
            e_msg+="\nDB: "+self.__db
        if self.__cmd<>None:
            e_msg+="\nCMD= "+self.__cmd
        if self.__par<>None:
            e_msg+="\nCMD= "+self.__par
        return e_msg
    def __repr__(self):
        e_repr={}
        e_repr['msg']=self.__msg
        e_repr['db']=self.__db
        e_repr['cmd']=self.__cmd
        e_repr['par']=self.__par
        return e_repr
    
class cmdError(dbError):
    """
        Utilizzato quando si verifica un errore mentre si processa uno statement sql
        Parametri:
        @msg Messggio di errore restituito da sqlite3
        @db Opzionale Nome del DB
        @cmd Opzionale Comando inviato al DB
        @par Opzionale: Riga dei parametri
    """
    pass
class accessError(dbError):
    """
        Utilizzato quando si non è possibile accedere al DB perchè lo si trova bloccato
        Parametri:
        @msg Messggio di errore restituito da sqlite3
        @db Opzionale Nome del DB
        @cmd Opzionale Comando inviato al DB
        @par Opzionale: Riga dei parametri
    """
    pass

#http://docs.python.org/2/library/sqlite3.html
#http://www.sqlite.org/pragma.html
#Pragma interessanti
# PRAGMA collation_list;
# PRAGMA compile_options;
# PRAGMA table_info(table-name);
# PRAGMA index_list(table-name);
# PRAGMA index_info(index-name);
# PRAGMA foreign_key_list(table-name);
# PRAGMA foreign_keys; 
# PRAGMA foreign_keys = boolean;
# PRAGMA encoding; 
# PRAGMA encoding = "UTF-8"; 
# PRAGMA encoding = "UTF-16"; 
# PRAGMA encoding = "UTF-16le"; 
# PRAGMA encoding = "UTF-16be";
# PRAGMA database_list;
# PRAGMA case_sensitive_like = boolean;

#self.cur = self.con:execute('pragma foreign_keys')
#print(self.cur:fetch())

def getTableSchemaFromCreateStr(createString):
    """
        Passandogli la stringa di create ritorna lo schema di una tabella.
        Utile per confrontare una tabella con una stringa di create.
        @createString Stringa per la creazione della tabella.
    """
    con = sqlite3.connect('')
    con.execute(createString)
    data=con.execute('SELECT name FROM sqlite_master WHERE type = "table"').fetchall()
    #print data
    name=None
    if len(data)<>2:
        raise prgError("Caso NON supportato.\n Sul temporaneo avrebbero dovuto esserci solo la tabella di cui ottenere lo schema e la tabella di sistema sqlite_sequence.\n Funzione da verificare:sqlitePool.getTableSchemaFromCreateStr")
    else:
        for x in data:
            tbName=str(x[0])
            if not tbName in ['sqlite_sequence']:
                name=tbName
                break
    if name==None:
        raise prgError("Caso NON supportato.\n Impossibile identificare la tabella di cui si dovrebbe leggere lo schema.\n Funzione da Verificare:sqlitePool.getTableSchemaFromCreateStr")
    else:
        data=con.execute('PRAGMA table_info(%s)'%name).fetchall()
    con.close()
    return (data, name)


class _dbConn(object):
    """
        Classe interna. Non Chiamare direttamente.
        Utilizzata da poolDB per la gestione delle connessioni.
        poolDB Quando ritorna una connessione, restituisce un oggetto di questa classe.
        
        E' importante ricordare di lanciare .free() quando si finisce di utilizzare una connessione
        per liberare la connessione e renderla disponibile al sistema.
    """
    def __init__(self,parent,id,dbconn):
        self.__used=False
        self.__conn=dbconn
        self.__id=id
        self.__parent=parent
    def isFree(self):
        """
            Ritorna True se la connessione non è utilizzata
        """
        return self.__conn==None
        #Ritorna se il cursore è ancora utilizzabile
    def free(self):
        """
            Libera il cursore che rimane aperto ma viene dichiarato libero.
            Va chiamata dal programma proprietario della connessione quando ha terminato di utilizzarla.
            Dopo la chiamata a free l'oggetto non sara più utilizzabile e la connessione sara tornata a 
            disposizione del poolDB che potrà riassegnarla.
        """
        self.__conn.commit()
        self.__parent.listcon[self.__id][0]=True
        self.__conn=None #Rende il cursore inutilizzabile. Una nuova richiasta di connessione non potrà riattivare quest'oggetto. Ma dovrà creare un nuovo oggetto che punta alla stessa connessione.
    #def.close():
        #Non IMPLEMENTARE LA CONNESSIONE NON DEVE ESSERE CHIUSA
        #SOLO MARCATA LIBERA DA self.free()
        
    #Seguono le funzioni di replica della connessione standard (solo poche sono implementate)    
    def commit(self):
        """
            Funzione di replica. Esegue la commit
        """
        if self.__conn==None:
            raise poolError("Errore: Tentativo di commit su connessione dichiarata libera")
        else:
            self.__conn.commit()
    def rollback(self):
        """
            Funzione di replica. Esegue il rollback
        """
        if self.__conn==None:
            raise poolError("Errore: Tentativo di rollback su connessione dichiarata libera")
        else:
            self.__conn.rollback()
    def execute(self, cmd, par=None):
        """
            Funzione di replica. esegue l'execute sulla connessione
        """
        data=None
        if self.__conn==None:
            raise poolError("Errore: Tentativo di execute su connessione dichiarata libera")
        else:
            if par==None:
                try:
                    data=self.__conn.execute(cmd)
                except sqlite3.OperationalError, msg:
                    msg=str(msg)
                    if msg==DATABASE_IS_LOCKED or msg==NOT_GOOD_DATABASE:
                        raise accessError(msg, self.__parent.getDbName(), cmd, par)
                    else:
                        raise cmdError(msg, self.__parent.getDbName(), cmd, par)
            else:
                try:
                    data=self.__conn.execute(cmd, par)
                except sqlite3.OperationalError, msg:
                    if msg==DATABASE_IS_LOCKED  or msg==NOT_GOOD_DATABASE:
                        raise accessError(msg, self.__parent.getDbName(), cmd, par)
                    else:
                        raise cmdError(msg, self.__parent.getDbName(), cmd, par)
        return data
    def executemany(self, cmd, par):
        """
            Funzione di replica. esegue l'executemany sulla connessione
        """
        #NON TESTATA
        data=None
        if self.__conn==None:
            raise poolError("Errore: Tentativo di executemany su connessione dichiarata libera")
        else:
            if par==None:
                try:
                    data=self.__conn.executemany(cmd)
                except sqlite3.OperationalError, msg:
                    msg=str(msg)
                    if msg==DATABASE_IS_LOCKED  or msg==NOT_GOOD_DATABASE:
                        raise accessError(msg, self.__parent.getDbName(), cmd, par)
                    else:
                        raise cmdError(msg, self.__parent.getDbName(), cmd, par)
            else:
                try:
                    data=self.__conn.executemany(cmd, par)
                except sqlite3.OperationalError, msg:
                    msg=str(msg)
                    if msg==DATABASE_IS_LOCKED  or msg==NOT_GOOD_DATABASE:
                        raise accessError(msg, self.__parent.getDbName(), cmd, par)
                    else:
                        raise cmdError(msg, self.__parent.getDbName(), cmd, par)
        return data
    def executescript(self, sql_script):
        """
            Funzione di replica. esegue l'executescript sulla connessione
        """
        #NON TESTATA
        data=None
        if self.__conn==None:
            raise poolError("Errore: Tentativo di executescript su connessione dichiarata libera")
        else:
            try:
                data=self.__conn.executescript(sql_script)
            except sqlite3.OperationalError, msg:
                msg=str(msg)
                if msg==DATABASE_IS_LOCKED  or msg==NOT_GOOD_DATABASE:
                    raise accessError(msg, self.__parent.getDbName(), sql_script,None)
                else:
                    raise cmdError(msg, self.__parent.getDbName(), sql_script,None)
        return data
    def create_function(self, name, num_params, func):
        """
            Funzione di replica. esegue il create_function
        """
        #NON TESTATA
        data=None
        if self.__conn==None:
            raise poolError("Errore: Tentativo di create_function su connessione dichiarata libera")
        else:
            try:
                data=self.__conn.create_function(name, num_params, func)
            except sqlite3.OperationalError, msg:
                msg=str(msg)
                if msg==DATABASE_IS_LOCKED or msg==NOT_GOOD_DATABASE:
                    raise accessError(msg, self.__parent.getDbName(), name,func)
                else:
                    raise cmdError(msg, self.__parent.getDbName(), name,func)
        return data


class poolDB(object):
    """
        Questa classe gestisce un pool di connessioni ad un db sqlite3
        
        Prima che ci siano connessioni si possono creare dei Preset. Un preset è una serie di Impostazioni di apertura e istruzioni pragma 
        ai quali si da un NOME di Preset.
        
        Quando si richiede una connessione la si può richiedere con un determinato preset. Se è presente una connessione libera con quel preset ne restituirà una
        Altrimenti poolDB si occupa di crearne una nuova ed applicare le impostazioni di preset.
        
        Importante al termine dell'utilizzo chiamare onCloseCheckConnectionLeak()
        che in pratica fa una verifica del programma e controllando che tutte le connessioni siano state 
        dichiarate libere prima di uscire. Se ci si è scordati una connessione aperta verrà lanciata un eccezione 
        sqlitePool.poolError
        
        In teoria la classe dovrebbe essere threadsafe
    """
    def __init__(self, dbFile):
        """
            Inizializza la classe
            se dbfile non esiste verrà creato alla prima richiasta di connessione.
        """
        #                                                stato ,  LinkSqlite , Nomepreset
        self.listcon=[] # Contiene una lista di liste [ [True  ,  ptsqlite,     "XX"        ],[False  ,  ptsqlite1,     "ZYX"] ....]
        self.listpreset=[] # Contiene un array di tuple (nome,["comando preset_0","comando preset_1"])
        self.lock = threading.Lock()
        self.dbFile=dbFile #nome del DB a cui connettersi
        self.pos=0#
        self.__defaultPreset=''
    def setDefaultPreset(self, prs):
        """
            Imposta un Preset di default. (se non viene chiamata per default non verrà utlizzato nessun Preset
            Nota può essere chiamata solo se non si sono connessioni attive, subito dopo aver definito i Preset
            @prs preset da impostare Deve essere una stringa non vuota. (Per impostare nessun preset (stringa vuota) basta evitare di chiamarla)
        """
        if len(self.listcon)>0:
            raise presetError("Errore: Ci sono connessioni attive. è possibile chiamare setDefaultPreset solo se non ci sono connessioni attive, quindi appena si è istanziato l'oggetto dopo la creazione dei preset")
        trovato=False
        for x in self.listpreset:
            if x[0]==prs:
                trovato=True
                break
        if not trovato:
            poolError("Errore: Preset < %s > non presente. Prima occorre creare il preset"%prs)
        self.__defaultPreset=prs
    def getDbName(self):
        """
            Ritorna il nome del DB
        """
        return self.dbFile
    def getDefaultPreset(self):
        """
            Ritorna il preset di default
        """
        return self.__defaultPreset
        
    def createPreset(self, namePreset, arrayPreset, dicopen):
        """
            Serve a creare le impostaziojni di preset
            @namePreset Nome del Preset
            @arrayPreset Un array di istruzioni pragma VOLATILI che saranno eseguite alla creazione della connessione
            @dicopen un dizionario di comandi da dare in apertura della connessione
            
            Per le istruzioni pragma fare riferimento a: #http://www.sqlite.org/pragma.html
            
            Questa funzione serve perchè non chiudendo le connessioni, queste possono avere dei preset precedenti
            e alcuni preset sono Tipici solo della connessione in corso e non del DB.
            Una di queste per esempio è "pragma foreign_keys=1". Su sqlite questa non è una proprieta del DB, ma della connessione in corso.
            Quindi Se voglio le foreign_keys attive su di un cursore devo creare un preset di questo tipo:
            
            Un esempio di utilizzo potrebbe essere questo:
            obj.createPreset(     "FK_LK:N_MTHREAD_Tout:10_ISOLATION:NONE" , ["PRAGMA foreign_keys=ON", "PRAGMA locking_mode = NORMAL"],{'check_same_thread':False, 'timeout':10,  'isolation_level':None}
            obj.setDefaultPreset( "FK_LK:N_MTHREAD_Tout:10_ISOLATION:NONE" )
            
            una successiva chiamata a obj.getConnection("FK_LK:N_MTHREAD_Tout:10_ISOLATION:NONE") oppure a 
            obj.getConnection() (visto che l'ho impostata per default) Mi restituirà una connessione con quel Preset.
            e se non sarà disponibile ne creerà una nuova e vi applicherà il Preset.
            
            Nota! Nell'array di preset è scorretto inserire PRAGMA o istruzioni non limitate alla connessione in corso. perchè
            ad ogni apertura di connessione farebbero un operazione che riguarda l'intero database.
        """
        if namePreset=="":
            raise presetError("Errore sulla chiamata sqlitePool.createPreset: Nome preset non indicato.")
        for x in self.listpreset:
            if x[0]==namePreset:
                raise presetError("Errore sulla chiamata sqlitePool.createPreset: Preset già presente. La ridefinizione di un Preset non è ammessa")
        #Su array preset non faccio alcun controllo.
        #Eventuali errori saranno rimandati dal DB quando si tenta di applicare il preset
        self.listpreset.append( (namePreset, arrayPreset, dicopen) )
        
    def __newConnection(self, preset):
        """
            Funzione interna. Viene chiamata se è necessario creare una nuova connessione. 
            Nel caso vi applica anche i Preset richiasti.
            @ Preset da applicare
        """
        #Nota per thread... Questa funzione non è lockato perche tanto viene chiamata solo da
        #getConnection già all'interno di un lock
        prs=None
        if preset<>"":
            opendic={}
            for x in self.listpreset:
                if x[0]==preset:
                    prs=x[1]
                    opendic=x[2]
                    break
            if prs==None:
                raise presetError("Errore in sqlitePool.getConnection -> self.__newConnection :Si sta cercando di utilizzare un Preset < %s > non definito"% preset)
            #CONNESSIONE CON PRESET
            try: 
                conn=sqlite3.connect(self.dbFile, **opendic)
            except sqlite3.OperationalError, e:
                raise accessError(str(e), self.dbFile,"sqlite3.connect",str(opendic))
            #E PRAGMA
            for x in prs:
                try: 
                    conn.execute(x)
                except sqlite3.OperationalError, e:
                    raise accessError(str(e), self.dbFile,x, None)
            conn.commit()
        else:
            #CONNESSIONE SEMPLICE
            conn=sqlite3.connect(self.dbFile)
        ret=_dbConn(self,len(self.listcon),conn)
        self.listcon.append([False, conn, preset])
        return ret
    def getConnection(self, preset=None):
        """
            Restituisce una connessione con un determinato preset e se non se non esiste chiama __newConnection(...) per crearla.
            @preset è il preset da utilizzare (lasciare a None per utilizzare il Preset di default
        """
        self.lock.acquire()
        itempos=0
        if preset==None:
            preset=self.__defaultPreset
        ret=None
        for x in self.listcon:
            if x[0] and x[2]==preset:
                ret=_dbConn(self,itempos,x[1])#
                x[0]=False
                break
            else:
                itempos+=1
        if ret==None:
            ret=self.__newConnection(preset)
        self.lock.release()
        return ret
    # ritorna true se il db è vuoto.
    def ifEmptyDB(self):
        """
            Funzione di utilità.
            Restituisce True se il DB e vuoto.
        """
        cn = self.getConnection()
        data=cn.execute('SELECT name FROM sqlite_master WHERE type = "table"').fetchone()
        if data==None or len(data)==0:
            empty=True
        else:
            empty=False
        cn.free()
        return empty
        
    def ListTable(self, ExcludeSystem=True):
        """
            Funzione di utilità.
            Ritorna la lista delle tabelle
            @ExcludeSystem=True Se impostato a False mostra anche le tabelle di sistema, altrimenti non le fa vedere
        """
        cn = self.getConnection()
        data=cn.execute('SELECT name FROM sqlite_master WHERE type = "table"').fetchall()
        #print data
        list=[]
        if data<>None:
            for x in data:
                tbName=str(x[0])
                #print tbName
                if ExcludeSystem:
                    if not tbName in ['sqlite_sequence']: #Aggiungere eventuali altre tabelle di sistema
                        list.append(tbName)
                else:
                    list.append(tbName)
        cn.free()
        return list
    def GetTableSchema(self, name):
        """
            Funzione di utilità.
            Ritorna lo schema di una tabella
            @name Nome della tabella di cui si richiede lo schema
        """
        cn = self.getConnection()
        data=cn.execute('PRAGMA table_info(%s)'%name).fetchall()
        cn.free()
        return data
    def GetTableIndex(self, name):
        """
            Funzione di utilità.
            Ritorna gli indici di una tabella
            @name Nome della tabella di cui si richiedono gli indici
        """
        cn = self.getConnection()
        data=cn.execute('PRAGMA index_list(%s)'%name).fetchall()
        cn.free()
        return data
    
    def GetTableFK(self, name):
        """
            Funzione di utilità.
            Ritorna le foreign_key di una tabella
            @name Nome della tabella di cui si richiedono le foreign_key
        """
        cn = self.getConnection()
        data=cn.execute('PRAGMA foreign_key_list(%s)'%name).fetchall()
        cn.free()
        return data
        
    def GetIndexInfo(self, name):
        """
            Funzione di utilità.
            Ritorna informazioni su un preciso indice
            @name Nome del'indice di cui si richiedono informazioni
        """
        cn = self.getConnection()
        data=cn.execute('PRAGMA index_info(%s)'%name).fetchall()
        cn.free()
        return data

    def checkTableSchema(self, createString, name=None, extendedInfo=False):
        """
            Confronta una tabella con una stringa di creazione per vedere se corrispondono.
            @createString       Stringa di creazione di una tabella
            @name=None          Nome della tabella con cui fare il confronto (se non inserito verrà usato lo stesso nome presente nella stringa di creazione)
            @extendedInfo=False False Ritorna True o False a seconda che la tabella coincida con lo schema.
                                True: Ritorna una tupla che contiene True/False e anche i dbscheme della stringa di creazione e della tabella. Infine il 4 parametro sarà il nome ricavato da dbSchema
            @return             Vedi @extendedInfo. Ritorna True/False Oppure una toupla (True/False,Schema.CreateString,Schema.Tabella,nomeCreateString
        """
        (s1, n1)=getTableSchemaFromCreateStr(createString)
        if name==None or name=="":
            name=n1
        s2=self.GetTableSchema(name)
#        print "S1", s1
#        print "S2", s2
        if s1==s2:
            ret=True
        else: 
            ret=False
        if extendedInfo:
            ret=(ret,s1, s2, n1)
        return ret
    
        
        
    def onCloseCheckConnectionLeak(self):
        """
            Verifica che tutte le connessioni siano state rilasciate
        """
        self.lock.acquire()
        openconnection=0
        for x in self.listcon:
            if not x[0]:
                openconnection+=1
            else:
                x[1].commit()
                x[1].close()
        self.lock.release()
        if openconnection>0:
            print openconnection
            raise poolError("Alcune Connessioni sono rimaste aperte. Numero Connessioni Aperte: "+str(openconnection))

    
tb_SECT = """create table if not exists sections (
    id_sect integer primary key autoincrement , 
    user text not null default "",
    section text not null default "",
    key text not null,
    ktype text not null
)"""

if __name__ == '__main__':
    import time
    start=time.time()
    a=poolDB("prova.db")
    #le connessioni di nome FK avranno le foreign_keys Attive
    a.createPreset("FK", ["PRAGMA foreign_keys=ON"], {})
    
    con2=a.getConnection("FK")
    data = con2.execute('pragma foreign_keys')
    print data.fetchall() , "VERIFICA INIZIALE DEVE ESSERCI UN 1"
    con2.free()
    for x in range(2):
        con1=a.getConnection()
        con2=a.getConnection("FK")
        data = con2.execute('pragma foreign_keys')
        print data.fetchall(), "DEVE ESSERCI UN 1"
        con3=a.getConnection()
        data = con3.execute('pragma foreign_keys')
        print data.fetchall() ,"DEVE ESSERCI UNO 0"
        con4=a.getConnection()
        con5=a.getConnection()
        con1.free()
        con2.free()
        con3.free()
        con4.free()
        con5.free()
    
    DEBUG=False #Disabilita il print per test velocità
    for x in range(1):
        con1=a.getConnection()
        con2=a.getConnection("FK")
        con3=a.getConnection()
        con4=a.getConnection("FK") #Apre una sesta connessione, Perchè connessione di tipo FK ne era stata aperta 1 sola
        con5=a.getConnection()
        con1.free()
        con2.free()
        con3.free()
        con4.free()
        con5.free()
    
    print a.ifEmptyDB()
    print a.ListTable()
    print a.ListTable(False)
    print a.GetTableSchema('pippo')
    print a.GetTableSchema('sections')
    print a.GetTableSchema('keyval')
    print "...", a.checkTableSchema(tb_SECT)
    print
    print a.GetTableIndex('sections')
    print a.GetTableIndex('keyval')
    print a.GetTableIndex('pippo')
    
    
    print a.GetIndexInfo('keyval_prow')
    print a.GetIndexInfo('keyval_kvalue')
    print a.GetIndexInfo('keyval_fkid_sect_kvalue')
    print a.GetIndexInfo('sections_u_s_k')
    print a.GetTableFK('sections')
    print a.GetTableFK('keyval')
    print a.GetTableFK('pippo')
    #con1=a.getConnection()    #!!!TOGLI Questa rem per verificare che la funzione a.onCloseCheckConnectionLeak restituisce un eccezione
    a.onCloseCheckConnectionLeak()    
    print "POOL",  time.time() - start       
    start1=time.time()
    for x in range(1):
        a=sqlite3.connect("prova.db")
        b=sqlite3.connect("prova.db")
        c=sqlite3.connect("prova.db")
        d=sqlite3.connect("prova.db")
        e=sqlite3.connect("prova.db")
    print "Normale", time.time() - start1       
