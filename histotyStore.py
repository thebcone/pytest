#!/usr/bin/python
#-*- coding: UTF-8 -*-

'''
historyStore 
@author: Claudio Bertuzzi thebcone@gmail.com
@license: LGPL v3 
@version: 0.09alfa
'''

''' CHANGELOG

0.09beta
    Iniziato test sul campo. Cioe sto provando ad inserirlo nell'applicazione per cui l'ho sviluppato.
0.08alfa
    Aggiunto test presenza parametri sui dizionari e inserito un meccanismo per farne overload su eventuali classi derivate
    revisionato condizioni di errore
    e aggiunto test per le eccezioni previste
    
0.07alfa
    messo qualche commento
    e ripulito il codice.
    autotest: Aggiunto test specifici per casi "particolari"

0.06alfa
    Sistemato e testato la funzione di update che presentava alcuni bug. Aggiunto e verificato la gestione
    di una modifica su " hlen " Per il resize della dimensione della coda e la modifica del
    tipo di coda. Riscritto funzioni per l'autotest in threading

0.05alfa
    Prima versione che utilizza il db temporaneo.
    l'history è concepita per essere inizializzata all'ingresso della form
    e chiusa alla fine.
    Quindi all'apertura del history vengono letti tutti i dati inerenti quel form
    (identificato da user e section) poi alla chiusura vengono salvati sul db reale 
    con un unica commit.
    L'auto test presenta ancora problemi nel funzionamento su thread, ma sembrano 
    solo errori e sviste. Non cose che richiedono di ripensare tutto.
    
    Fatto in questo modo 2 form che utilizzano lo stesso user e sessione diventano concorrenti.
    entrambe riusciranno a salvare i propri dati, ma chi chiude per ultimo li sovrascrive. 
    Mi sembra anche che sia giusto così.
    
0.04alfa
    Versione di transizione Modifiche sperimentali a 0.03alfa
    per trovare una soluzione al problema
    
0.03alfa
    Pulizia del codice e scrittura di un test per il funzionamento concorrente.
    Fallito miseramente!!!
    Si sono resi necessarie modifiche al modulo sqlitePool per L'apertura del db 
    che consentisse l'utilizzo di una connessione da diversi thread.
    Rilevati comunque gravi Problemi di lock sul DB. L'unica maniera per risolverli sarebbe stato
    fare le commit ad ogni istruzione, ma un test in quel senso rallentava
    troppo il programma (a meno di un cambiamento del pragma che gestisce la velocità del
    commit a scapito della sicurezza dei dati)
   
0.02alfa
    Il programma ha raggiunto lo stadio di funzionamento accettabile.
    Si è reso necessario medificare sqlitePool per la gestione dei "preset"
0.01alfa
    Prototipo di programma con qualche prova di creazione db, inserimento e recuro dati
'''
import sqlitePool
import sqlite3

# Definizione classi per eccezioni.
class Error(Exception):
    """ Classe base degli errori di sqlitePool.
        Da non usare dirattamente.
    """
    pass

class prgError(Error):
    """
        Chiamata quando viene intercettato un orrore logico del programma. Un test su una situazione che
        non dovrebbe capitare, ma è capitata. In questo codice viene chiamata. Per esempio una chiamata alla 
        funzione con un tipo di chiave non supportata
    """
    pass


#DEFINIZIONE DB
# table: sections
# user gestisce un eventuale funzionamento multiutente sulla stessa sessione
# section dovrebbe corrispondere col nome della form per la quale si utilizza l'history
# key dovrebe corrispondere al nome del riquadro di edit di cui salvare l'istory
# ktype. Può Assumere i valori "LIST" o "UNIQUELIST". 
#   LIST Ammette il salvataggio di valori doppi. quindi è adatto a tenere uno storico 
#       degli inserimenti anche se i valori sono sempre uguali.
#   UNIQUELIST: (Impostazione di default) conserva solo un esemplare per ogni valore.
#       se un valore viene inserito più di una volta, viene semplicemente spostato in cima
#       al cronologico degli inserimenti (lo si vede richiamando l'history con l'ordine "RevIns"
# hlen contiene la dimensione della coda di history
tb_SECT = """create table if not exists sections (
    id_sect integer primary key autoincrement , 
    user text not null default "",
    section text not null default "",
    key text not null,
    ktype text not null,
    hlen integer default 50
)"""
idx_SECT='CREATE UNIQUE INDEX if not exists sections_u_s_k ON sections(user,section,key)'
# table: keyval (costituisce il corpo di sections)
# fkid_sect è la foreign key su setions
# prow é un numero progressivo d'ordine con cui vengono inserite le history. 
#   la riga con valore più alto è l'ultima inserita (o aggiornata, nel caso
#   di una UNIQUELIST in cui sia stato ripetuto un valore salvato in precedenza).
# ftype Per ora ha un solo valore "HIST" ed è indicativo del programma che sta 
#   gestendo gli inserimenti. In futuro intendo salvare su questo DB altre informazioni
#   riguardanti la form (per esempio posizione e dimensioni). Intendo distinguerle grazie
#   a questo campo.
# level: Per ora non utilizzato. In futuro altri prog che si appoggiano a questo DB potrebbero
#   salvare informazioni in modo nidificato per salvare per esempio cose del tipo {key1:{altrakey:"valore"}}
# k1value: Contiene l'informazione di history. (quando ftype=="HIST", vedi ftype)
# k2value: Npon utilizzato. Per il futuro, su particolari ftype da definire è previsto che possa essere
#   utilizzato come parte valore di un dizionario, dove k1value è la chiave. 
tb_KEYVAL = """create table if not exists keyval (
    id_key integer primary key autoincrement , 
    fkid_sect integer,
    prow integer default 0,
    ftype text not null,
    level integer not null default 0,
    k1value text not null default "",
    k2value text not null default "",
    foreign key (fkid_sect) references sections (id_sect) on delete cascade on update cascade
)"""
idx_KEYVAL1='CREATE INDEX if not exists keyval_fkid_sect_k1value ON keyval(fkid_sect,k1value)'
idx_KEYVAL2='CREATE INDEX if not exists keyval_k1value ON keyval(k1value)'
idx_KEYVAL3='CREATE INDEX if not exists keyval_prow ON keyval(prow)'


#NOTA SUL DB TEMPORANEO
#User e section non li uso. Il cursore è attivo su un unico user e section
#id_sect per i nuovi sarà virtuale e ricalcolato al momento dell'insert
mem_SECT = """create table sections (
    id_sect integer, 
    key text not null,
    ktype text not null,
    hlen integer default 50
)"""
mem_idx_SECT='CREATE UNIQUE INDEX if not exists sections_k ON sections(key)'

#l'ID non mi interessa. fkid_sect==id_sect, per i nuovi però sarà "virtuale" e ricalcolato al momento dell'insert
#In questo momento level e k2value non mi interessano. Non sono toccati da questa procedura
#anche ftype non lo utilizzo. Per la roba gestita da questa classe sarà sempre "HIST"
mem_KEYVAL = """create table keyval (
    fkid_sect integer,
    prow integer default 0,
    k1value text not null default ""
)"""
mem_idx_KEYVAL1='CREATE INDEX if not exists keyval_fkid_sect_k1value ON keyval(fkid_sect,k1value)'
mem_idx_KEYVAL2='CREATE INDEX if not exists keyval_k1value ON keyval(k1value)'
mem_idx_KEYVAL3='CREATE INDEX if not exists keyval_prow ON keyval(prow)'


# Attenzione sqlitePool VS apertura diretta
# Attualmente historyStore si appoggia alla lib sqlitePool. Un modulo che ho scritto per riutilizzare le connessioni a sqlite3 già aperte
# Inizialmente riutilizzare una connessione migliorava di un fattore 10 le prestazioni rispetto all'operazione di apertura di un nuovo DB.
# L'aggiunta di ulteriori controlli che si sono resi necessari probabilmente ha peggiorato questa situazione.
# L'apertura diretta di un DB sqlite è comunque un operazione velocissima e probabilmente se non in casi molto particolari,
# il piccolo aumento di prestazioni che potrebbe dare sqlitePool non ne giustifica l'utilizzo rispetto ad un apertura diretta.
# In questo modulo lo utilizzo perchè ormai sqlitePool l'ho scritto e volevo collaudarlo in previsione di riutilizzarlo per
# connessioni ad altro DB (dove magari con un server che deve rispondere ed una connessione di rete la differenza di prestazioni
# diventa significativa)
#
# Il difetto fondamentale è che le connessioni a sqlite3 possono essere utilizzate su thread diversi solo se sqlite3 
# è stato compilato per permetterlo (e questo è abbastanza probabile, ma non certo). Ad ogni modo le connessioni aperte
# non possono essere portate su processi diversi. L'ultimo limite a fronte di una differenza di prestazioni così poco
# significativa non mi piace molto.
#
# e il guadagno sarebbe di poterlo utilizzare anche su 
# Prevedo quindi di eliminare l'utilizzo di sqlitePool da questo programma. L'impatto della modifica dovrebbe essere minimo.
# un insignificante peggioramento delle prestazioni a favore della possibilità di utilizzare history store su processi diversi.

def openHistoryDb(dbname):
    """
        Apre (e se necessario, lo inizializza) il db di history chiamando sqlitePool
        con le impostazioni di preset adeguate.
    """
    # NOTA! check_same_thread':False richide che sqlite3 sia compilato per supportare il supporto dei thread (quasi certo)
    # L'utilizzo di sqlitePool non è veramente necessario. (vedi sopra "Attenzione sqlitePool VS apertura diretta")
    db=sqlitePool.poolDB(dbname)
    #Attivo le foreign_keys, imposto un timeout per i look a 30 e isolation_level=None perchè ho bisogno di controllarlo io
    db.createPreset("HISTORY", ["PRAGMA foreign_keys=ON", "PRAGMA locking_mode = NORMAL"],{'check_same_thread':False, 'timeout':0.5,  'isolation_level':None} )
    db.setDefaultPreset("HISTORY") #Per default imposto di aprire le connessioni con il preset HISTORY
    if db.ifEmptyDB(): #Se la tabella non esiste la creo
        initDB(db)
    return db

def initDB(hPool):
    """
        Inizializza il DB Da chiamare la prima volta per creare il db dell'history.
        Al momento come paramentro richiede un oggetto sqlitePool, ma è banale riadattarlo
        ad una connessione diretta
    """
    cn = hPool.getConnection()
    # ### DROP IN ORDINe INVERSO DI CREAZIONE, SOLO PER DUBUG
    ##keyval
    cn.execute('drop index if exists keyval_fkid_sect_k1value')
    cn.execute('drop index if exists keyval_k1value')
    cn.execute('drop index if exists keyval_prow')
    cn.execute('drop table if exists keyval')
    ##sections
    cn.execute('drop index if exists sections_u_s_k')
    cn.execute('drop table if exists sections')
    # ### FINE AREA DROP
    cn.execute(tb_SECT)
    cn.execute(idx_SECT)
    cn.execute(tb_KEYVAL)
    cn.execute(idx_KEYVAL1)
    cn.execute(idx_KEYVAL2)
    cn.execute(idx_KEYVAL3)
    #print cn.execute('PRAGMA compile_options').fetchall()
    cn.free()
    

CONST_DEFA_HLEN=50
CONST_KY_HIST="HIST" # L'identificativo di questa classe sulla tabella keyval. Se il campo ftype non sarà "HIST" non è di pertinenza di questa classe.
CONST_SQ_KEY_LIST="LIST"
CONST_SQ_KEY_UNIQUELIST="UNIQUELIST"
LIST_SEQUENCE_KEY=(CONST_SQ_KEY_LIST,  CONST_SQ_KEY_UNIQUELIST)    
DEFA_SQ_KEY=CONST_SQ_KEY_UNIQUELIST

HORD={'Ins':"prow", 
        "RevIns":"prow DESC", 
        "Alfa":"k1value", 
        "RevAlfa":"k1value DESC"
    }
CONST_DEFA_HORD=HORD["RevIns"]
    
class storeHistory(object):
    """ Questa Classe serve a gestire il salvataggio dei dati di history di una form.
        Un oggetto di questa classe va inizializzata all'apertura del form e chiuso
        in fase di salvataggio dati oppure di chiusura del form. 
        I dati saranno effettivamente salvati solo alla chiamata del metodo close. Fino ad allora
        resteranno in memoria.
    """
    def __initMemDB(self):
        """
            Funzione interna - Inizializza il db temporaneo leggendo i dati presenti dal db normale
            per lo user e la sezione specificati alla creazione della classe
            Viene chiamata dall'init oppore da self.updateDB Per rileggere un nuovo cursore 
        """
        self.__con = sqlite3.connect('')
        self.__con.execute(mem_SECT)
        self.__con.execute(mem_KEYVAL)
        self.__con.execute(mem_idx_SECT)
        self.__con.execute(mem_idx_KEYVAL1)
        self.__con.execute(mem_idx_KEYVAL2)
        self.__con.execute(mem_idx_KEYVAL3)
        data=self.__cn.execute('select id_sect,key,ktype,hlen from sections where user=? and section=?', (self.__defaUser, self.__defaSection) ).fetchall()
        data1=self.__cn.execute('select fkid_sect,prow,k1value from keyval,sections where fkid_sect=id_sect and user=? and section=?', (self.__defaUser, self.__defaSection) ).fetchall()
        self.__cn.commit() #Libera il db
        self.__con.executemany('insert into sections (id_sect,key,ktype,hlen) values (?,?,?,?)', data)
        self.__con.executemany('insert into keyval (fkid_sect,prow,k1value) values (?,?,?)', data1)
        
    def __init__(self, historyPool,user, section,**kwargs):
        """
            Inizializza un oggetto della classe.
            La funzione principale oltre all'inizializzazione delle variabili è la creazione del temporaneo
            di history. Il lavoro effettivo verra fatto tutto sul temporaneo Per essere scritto sul DB solo alla fine
            
            I parametri obligatori sono user e section
            @user è l'eventuale utente del programma (in una gestione multiuser all'interno dello stesso login)
            se il programma non prevede una gestione multiuser si può usare "" oppure una qualsiasi stringa FISSA,
            per esempio "ALL" o "STD"
            @section è la sezione. Generalmente dovrebbe essere il nome della form di cui salvare l'history
            @defa_hlen= un intero positivo - Default=50
                E' la lunghezza della coda di history. Rappresenta solo un default per le nuove chiavi
                e può essere sovrascritto nel momento che i crea effettivamente una nuova chiave.
            @dafa_sqType= uno di questi valori "LIST","UNIQUELIST" - Default="UNIQUELIST"
                Indica il tipo di lista che verrà usata per le nuove chiavi. è solo un impostazione di default
                al momento di creare una nuova chiave lo si può sovrascrivere.
            @defa_hord= Uno di questi valori "Ins","RevIns","Alfa","RevAlfa" - Default="RevIns"
                rappresenta l'ordine di default con il quale saranno restituiti i dati da HistoryLoadKey()
        """
        self.__set_ck_dictKey() #Inizializza le variabili per il check dei dizionari. Derivare questa funzione o quelle al suo interno per modificare i check sui dizionari in eventuali classi figlio
        self.__Hpool= historyPool
        self.__cn=self.__Hpool.getConnection()
        self.__defaUser=user
        self.__defaSection=section
        
        #Verifica che su kwargs non ci siano nomi errati. La tupla dei nomi ammessi è stata generata in __set_ck_dictKey (eventualmente in classi figlie può essere necessario farne l'overload)
        for key in kwargs.keys():
            if not key in self.__INIT__KWARGS_ADMITTED:
                self.__cn.free()
                raise prgError("Errore: parametro sconosciuto < %s >. Ammessi: %s"%(key, str(self.__INIT__KWARGS_ADMITTED)))
       
        #dafa_sqType - default tipo sequenza
        if "defa_ktype" in kwargs:
            if kwargs["defa_ktype"] in LIST_SEQUENCE_KEY:
                self.defaSeqKey=kwargs["defa_ktype"]
            else:
                self.__cn.free()
                raise prgError("Errore: Parametro defa_ktype (default tipo sequenza) = < %s > errato. Ammessi: %s"%(str(kwargs["defa_ktype"]), str(LIST_SEQUENCE_KEY)))
        else:
            self.defaSeqKey=DEFA_SQ_KEY    
        #defa_hlen - Lunghezza history di default
        if "defa_hlen" in kwargs:
            self.defaHistorLen=kwargs["defa_hlen"]
        else:
            self.defaHistorLen=CONST_DEFA_HLEN
        #defa_hord - Sequenza di default
        if "defa_hord" in kwargs:  
            if kwargs["defa_hord"] in HORD.keys():
                self.defaOrd=HORD[kwargs["defa_hord"]]
            else:
                self.__cn.free()
                raise prgError("Errore: Parametro defa_hord (ordine di default) < %s > errato. Ammessi: %s"%(str(kwargs["defa_hord"]), str(HORD.keys())))
        else:
            self.defaOrd=CONST_DEFA_HORD
        
        self.__initMemDB()#Crea il cursore in memoria. 
        #Salvo forzature è previsto che Il cursore venga ricopiato sul DB alla chiusura (/salvataggio) della maschera. Con una unica commit.
    def __Set_Init__set_test_kwargs(self):
        """
            Se si deriva la classe e si aggiungono parametri all'init occorre fare un overload di questa classe
            e cambiare la riga self.__INIT_KWARGS_ADMITTED aggiungendo i nuovi valori
            Chiamata all'interno della funzione __check_init_kwargs
        """
        self.__INIT__KWARGS_ADMITTED=('defa_hlen', 'defa_ktype', 'defa_hord')
    def __set_ck_dictKey(self):
            """
                Funzione interna. Crea le variabili per il check dei dizionari kwargs.
                Serve Farne l'overload se si vuole aggiungere qualche chiave.
            """
            if not hasattr(self, '__check_init_kwargs'):
                self.__Set_Init__set_test_kwargs()
                self.__createNewKeyIfNotExistAndGetID_set_test_kwargs()
                self.__check_init_kwargs=True #Evita di venire chiamata più volte
            
    def debugPrintSection(self,  key):
        """
            Come indicato dal nome è una funzione di debug.
            Stampa il contenuto di una key.
            HEADER
            -->body.
            Non è multithread... come minimo almeno i print andrebbero lockati, altrimenti possono sovrapporsi.
            Ancora meglio sarebbe lockare tutta la funzione in modo da avere l'output di una singola chiave consecutivo
            Non li ho messi perchè tanto è una funzione di debug. Un print di quel genere all'interno di un thread 
            avrebbe poco senso.
            Le funzioni di look comunque (non collaudate) sono presenti ma commentate.
        """
        #self.__lk = threading.Lock()
        #self.__lk.acquire()
        data=self.__con.execute('select * from sections where key=?', [key]).fetchall()
        for header in data:
            print header
            data=self.__con.execute('select * from keyval where fkid_sect=? order by prow', [header[0]]).fetchall()
            for row in data:
                print "-->", row
        #self.__lk.release()
    def __createNewKeyIfNotExistAndGetID_set_test_kwargs(self):
        """
            Se si deriva la classe e si aggiungono parametri all'init occorre fare un overload di questa classe
            e cambiare la riga self.__INIT_KWARGS_ADMITTED aggiungendo i nuovi valori
            Chiamata all'interno della funzione __check_init_kwargs
        """
        self.__INIT__createNewKey_ADMITTED=('hlen', 'ktype')
        
    def createNewKeyIfNotExistAndGetID(self, key, **kwargs):
        """
            Crea o modifica una chiave. In caso di modifica alle dimensioni dell'history (riduzione)
            o modifica del tipo di history Con passaggio da "LIST" a "UNIQUELIST" modifica anche il body
            della chiave per adattarlo alle nuove impostazioni.
            @ key Nome chiave da creare/modificare e ritornare
            @ ktype="LIST" or "UNIQUELIST" Parametro opzionale che specifica il tipo di chiave
                Per le nuove chiavi lo imposta e baste.
                Se la chiave è gia presente, lo verifica ed imposta il nuovo valore. Nel caso ci sia un passaggio da "LIST" a "UNIQUELIST"
                elimina tutti gli eventuali valori doppi
            @ hlen=num Parametro opzionale Per le nuove chiavi imposta la lunghezza della nuova chiave a num. Se 
                una chiave è già presente imposta il nuovo valore e se la dimensione è stata ridotta cancella 
                le eventuali chiavi in eccesso.
            @ return Ritorna (id_sect,ktype,hlen) della chiave.
        """
        #Effettua il check che non ci siano nomi sbagliati su kwargs
        for k in kwargs.keys():
            if not k in self.__INIT__createNewKey_ADMITTED:
                raise prgError("Errore: parametro sconosciuto < %s >. Ammessi: %s"%(k, str(self.__INIT__createNewKey_ADMITTED)))

        data=self.__con.execute('select id_sect,ktype,hlen from sections where key = ?',[key]).fetchone()
        if data==None:
            #NUOVA CHIAVE
            #Per ktype e hlen se non sono stati forniti mi prendo un default
            if 'ktype' in kwargs:
                ktype=kwargs['ktype']
                if not ktype in LIST_SEQUENCE_KEY:
                    raise prgError("Errore: Parametro ktype (default tipo sequenza) = < %s > errato. Ammessi: %s"%(str(kwargs["ktype"]), str(LIST_SEQUENCE_KEY)))
            else:
                ktype=self.defaSeqKey
            if 'hlen' in kwargs:
                hlen=kwargs['hlen']
                if hlen<0:
                    hlen=0
            else:
                hlen=self.defaHistorLen    
            newid=self.__con.execute('select COALESCE(max(id_sect),0)+100 FROM sections').fetchone()[0] #Per evitare casini con i valori importati dal DB ho preferito non avere un indice autoincrementante sul cursore. Quindi il nuovo ID  me ne calcolo a mano
            self.__con.execute('insert into sections(id_sect, key, ktype , hlen) values (?,?,?,?)',(newid, key, ktype, hlen) )            
        else:
            #CHIAVE GIA PRESENTE
            newid=data[0]
            s_update=""
            if 'ktype' in kwargs:
                if kwargs['ktype']<>data[2]:
                    # è presente un valore ktype ed è diverso dal precedente
                    if kwargs['ktype'] in LIST_SEQUENCE_KEY:
                            s_update=" ktype = '"+kwargs['ktype']+"' "  #Imposta la stringa con cui fare l'update di ktype
                            if kwargs['ktype']==CONST_SQ_KEY_UNIQUELIST:
                                #Se passo da un tipo "LIST" ad un "UNIQUELIST" tolgo le chiavi doppie
                                self.__con.execute('delete from keyval where fkid_sect=? and prow not in (select max(prow) as prow from keyval where fkid_sect=? group by k1value)',[newid,newid])
                            #else:Da "UNIQUELIST" a "LIST" non è necessario fare nulla.
                    else:
                        raise prgError("Errore: Parametro ktype (default tipo sequenza) = < %s > errato. Ammessi: %s"%(str(kwargs["ktype"]), str(LIST_SEQUENCE_KEY)))
                ktype=kwargs['ktype'] #aggiorna il valore ktype. E' fuori dall'IF perche tanto anche se fossero diversi quello buono da ritornare è quello letto da kwargs. Quello sul DB verrà aggiornato.
            else:
                #Se ktype non è nel dic kwargs me lo leggo dal DB
                ktype=data[1]
            if 'hlen' in kwargs: 
                if kwargs['hlen']<0:
                    kwargs['hlen']=0
                if kwargs['hlen']<>data[1]:
                    #è presente un valore hlen ed è diverso dal precedente
                    #preparo la stringa di update per hlen (è diversa per la virgola, se era gia presente un upgrade per ktype
                    if s_update=="":
                        s_update=" hlen = "+str(kwargs['hlen'])+" "
                    else:
                        s_update+=", hlen = "+str(kwargs['hlen'])+" "
                    #Se il nuovo hlen fosse minore del precedente cancello le righe in eccesso
                    if kwargs['hlen']<data[1]:
                        self.__con.execute('delete from keyval where fkid_sect=? and prow not in (select prow from keyval where fkid_sect=? order by prow DESC limit ?)', (newid,newid,kwargs['hlen']) )
                hlen=kwargs['hlen']#aggiorna il valore hlen. E' fuori dall'IF perche tanto anche se fossero diversi quello buono da ritornare è quello letto da kwargs. Quello sul DB verrà aggiornato.
            else:
                #Se hlen non è nel dic kwargs me lo leggo dal DB
                hlen=data[2]
            if s_update<>"":
                #Se c'è stata una modifica di ktype o hlen è stata creata una stringa di update che vado ad eseguire
                self.__con.execute('update sections set '+s_update+'where id_sect=?', [newid] )
        return (newid, ktype, hlen) #ritorno sempre le info della chiave indipendentemente dal fatto sia nuova o fosse già presente.
        
    def HistoryLoadKey(self, key,ord='RevIns'):
        """
            Legge una chiave dall'history
            @ key chiave da leggere
            @ ord Ordine da utilizzare
            @ return Ritorna una tupla di 2 elementi.
             il primo elemento è una stringa con l'ultimo valore inserito
             il secondo elemento e un array dell'history che rispetta l'ordine richiasto
        """
        if ord in HORD:
            ord=HORD[ord]
        else:
            raise prgError("Errore: Parametro ord valore < %s > non ammesso. Ammessi: %s"%(ord, str(HORD.keys())))
        qy=self.__con.execute('select id_sect from sections where key = ?', [key]).fetchone()
        last=''
        hist=[]
        if qy<>None:
            id=qy[0]
            qy=self.__con.execute('select k1value from keyval where fkid_sect=? order by prow DESC', [id]).fetchone()
            if qy<>None:
                last=qy[0]
                qy=self.__con.execute('select k1value from keyval where fkid_sect=? order by '+ord, [id]).fetchall()
                hist = list(str(x[0]) for x in qy)
        return (str(last), hist)
        
    def update(self, key, value, **kwargs):
        """ Salva un nuovo valore sull'history. Nota Se la chiave dell'history non esiste viene creata.
            Se gli vengono passati i parametri opzionali ktype=... o hlen=... Vengono passati alla funzione
            createNewKeyIfNotExistAndGetID (vedi) che crera una nuova chiave o fara l'update di una chiave già 
            esistenete  in modo che rispetti questi parametri
            @key è la chiave dell'history
            @value è il valore da inserire
            @ktype= Parametro opzionale. Passato a createNewKeyIfNotExistAndGetID (vedi)
            @hlen= Parametro opzionale. Passato a createNewKeyIfNotExistAndGetID (vedi)
        """
        (id, ktype, hlen)=self.createNewKeyIfNotExistAndGetID(key,**kwargs)
        if ktype=="UNIQUELIST":
            #Cancella un eventuale uguale inserito in precedenza, per inserirlo poi di nuovo come ultimo elemento
            self.__con.execute('delete from keyval where fkid_sect=? and k1value=?', (id, value)) #cancella un eventuale chiave precedente che sarà appesa alla fine
        #ora "LIST" e "UNIQUELIST" le tratto uguali perchè l'eventuale doppione di un UNIQUELIST l'ho appena eliminato e andrò ad aggiungere in coda il nuovo elemento.
        #Nota! sono su un TEMPORANEO e la frammentazione del DB non mi preoccupa perchè durante la funzione close con la reale scrittura sul DB
        #le righe vengono riutilizzate e non cancellate e riscritte
        nrow=self.__con.execute('select count(*) from keyval where fkid_sect=?', [id]).fetchall()[0][0]
        if nrow>=hlen:
            # Se il numero di righe nrow è uguale (o maggiore, ma a parte per interventi esterni non dovrebbe mai essere maggiore)
            # al massimo_numero_di_righe ammesse, mantengo solo le ultime massimo_numero_di_righe -1 . -1 Perchè l'insert successivo le porterà ancora a massimo_numero_di_righe
            self.__con.execute('delete from keyval where fkid_sect=? and prow not in (select prow from keyval where fkid_sect=? order by prow DESC limit ?)', (id,id,hlen-1) )
        #inserisco la nuova riga di historiy
        self.__con.execute('insert into keyval (fkid_sect,prow,k1value) values (?,(SELECT COALESCE(max(prow),0) FROM keyval where fkid_sect=?)+5, ?)', (id,id,value))
    def __saveCursor(self):
        """
            Funzione interna. Questa funzione si occupa di rileggere il temporaneo e di salvare i valori sul
            DB. Esegue il tutto all'interno di una transazione e dovrebbe fare un rollback in caso di fallimento
            In caso di uso concorrente la funzione potrebbe anche fallire in timeout. A parte che con un uso
            MANUALE dovrebbe essere pratichamente impossibile che si verifichi, ho semplicemente scelto di ignorare il problema.
            In fondo non si tratta di salvare dei dati ma un history dei valori inseriti e se fallisce "allora, pazienza".
        """
        data=self.__con.execute('select id_sect,key,ktype,hlen from sections').fetchall()
        try: 
            self.__cn.execute("BEGIN EXCLUSIVE TRANSACTION")
            for x in data:
                #Guardo se la chiave esiste già
                old=self.__cn.execute("select id_sect from sections where user=? and section=? and key=?", (self.__defaUser, self.__defaSection, x[1])).fetchone()
                if old==None:
                    #Si tratta di una chiave nuova
                    self.__cn.execute("insert into sections (user, section, key, ktype, hlen) values (?,?,?,?,?)", (self.__defaUser, self.__defaSection, x[1], x[2], x[3]))
                    newid=self.__cn.execute('select last_insert_rowid()').fetchone()[0]
                    body=self.__con.execute('select prow,k1value from keyval where fkid_sect=? order by prow', [x[0]]).fetchall()
                    for y in body:
                        self.__cn.execute("insert into keyval (fkid_sect,  prow,  ftype, k1value) values (?, ?, ?, ?)", (newid, y[0], CONST_KY_HIST, y[1]))
                else:
                    #La chiave esiste già l'id e in old[0]
                    self.__cn.execute("update sections set ktype=? , hlen=? where id_sect=?",  (x[2], x[3],old[0]) )
                    #Per non frammentare troppo il DB faccio degli update anziche cancellare e riscrivere
                    #Quindi Inizio col leggermi gli ID precedenti... e poi li ciclo per updatarli...
                    #Se mi rimangono degli ID li cancello
                    #Altrimenti se li finisco Passo al semplice insert
                    oldbody=self.__cn.execute("select id_key from keyval where fkid_sect=?", [old[0]]).fetchall()
                    newbody=self.__con.execute('select prow,k1value from keyval where fkid_sect=? order by prow', [x[0]]).fetchall()
                    maxrow=len(newbody)
                    row=0
                    for y in oldbody:
                        if row<maxrow:
                            # Ciclo all'interno di oldbodi e faccio l'update dei valori
                            nbd=newbody[row]
                            self.__cn.execute("update keyval set prow=?,ftype=?,k1value=? where id_key=?",  (nbd[0], CONST_KY_HIST,nbd[1], y[0]) )    
                            row+=1
                        else:
                            #Se arrivo quà significa che le righe del temporaneo sono finite. Quindi devo cancellare dal DB eventuali altre righe
                            self.__cn.execute("delete from keyval where id_key=? ", (y[0], ) )    
                    while row<maxrow:
                        #Se qrrivo qua dentro il temporaneo conteneva un numero di righe superiore rispetto al DB
                        #In questo ciclo aggiungo le righe rimanenti
                        nbd=newbody[row]
                        self.__cn.execute("insert into keyval (fkid_sect,  prow,  ftype, k1value) values (?, ?, ?, ?)", (old[0], nbd[0],CONST_KY_HIST,nbd[1]))
                        row+=1
            self.__cn.execute("COMMIT TRANSACTION")
        except sqlite3.OperationalError , e:
            print "DATABASE LOCKED, saltato Save history"
            print str(e)
    def updateDB(self):
        self.__saveCursor()
        self.__con.close
        self.__initMemDB() #Riapre con i dati aggirnati

    def close(self):
        self.__saveCursor()
        self.__con.close()
        self.__cn.free() # la commit non dovrebbe essere necessaria, perche self.saveCursor dovrebbe averla fatta. Comunque dentro free viene rifatta

import threading

class TestTread(threading.Thread):
    tuttoOK=True
    def __init__(self, name, db, user, sect, len, numsect, numvalue):
        threading.Thread.__init__(self)
        self.t_name = name
        self.db=db
        self.user=user
        self.sect=sect
        self.len=len
        self.numsect = numsect
        self.numvalue = numvalue
    #TestTread("-- T1 -- >", p1, 10, 60)
    def run(self):
        if TestTread.tuttoOK:
            try:
                self.hist = storeHistory(self.db, self.user, self.sect, defa_hlen=self.len)
                for y in range(self.numsect):
                    print self.t_name, y
                    for x in range(self.numvalue):                
                        self.hist.update("key%2d"%y, "%s value%3d"%(self.t_name, x))
                        #print "........ pass", self.t_name, y, self.t_name, x
                        if TestTread.tuttoOK==False:
                            break
                    if TestTread.tuttoOK==False:
                        break
            except Exception, e:
                    TestTread.tuttoOK=False
                    print str(e)
                    print "errore in", self.t_name 
                    raise e
            finally:
                if TestTread.tuttoOK:
                    print self.t_name, "FINITO , FINITO , FINITO , FINITO , FINITO , FINITO"
                    self.hist.close()
                else:
                    print self.t_name
                    print self.t_name , "break anticipato"
                
                

import sys
def check_loadHistory(msg, touple_returned, touple_expected):
    ret_last=touple_returned[0]
    ret_hist=touple_returned[1]
    exp_last=touple_expected[0]
    exp_hist=touple_expected[1]
    if ret_last<>exp_last:
        print "(LAST,HIST)=HistoryLoadKey() - errore ritorno LAST"
        print "test:", msg
        print "era atteso:",  exp_last
        print "Restituito:",  ret_last
        sys.exit(1)
    if ret_last<>exp_last:
        print "(LAST,HIST)=HistoryLoadKey() - errore ritorno HIST"
        print "test:", msg
        print "era atteso:",  exp_hist
        print "Restituito:",  ret_hist
        sys.exit(1)
    print "OK test:", msg
    print "LAST", ret_last
    print "HIST", ret_hist
    print

def checkRevDB(dbn, msg, user,  sect, key, array, tipo, hlen):
    tmpcon=sqlite3.connect(dbname, timeout=60)
    query=tmpcon.execute("select id_sect,hlen,ktype from sections where user = ? and section = ? and key=?", (user, sect, key)).fetchone()
    if query==None:
        print "ERRORE CHEK DB SAVED DATA: (", user,",",   sect,",", key,")"
        print "Test: ", msg
        print "MANCA LA CHIAVE", user,",",   sect,",", key,
        sys.exit(1)
    if hlen<>query[1]:
        print "ERRORE CHEK DB SAVED DATA: SULLA LUNGHEZZA LISTA (", user,",",   sect,",", key,")"
        print "Test: ", msg
        print "Atteso  :", hlen
        print "presente:", query[1]
        sys.exit(1)
    else:
        hlen=query[1]
    if tipo<>query[2]:
        print "ERRORE CHEK DB SAVED DATA: SULLA TIPO LISTA (", user,",",   sect,",", key,")"
        print "Test: ", msg
        print "Atteso  :", tipo
        print "presente:", query[2]
        sys.exit(1)
    else:
        ktype=query[2]
    id=query[0]
    query=tmpcon.execute('select k1value from keyval where fkid_sect=? order by prow DESC', [id]).fetchall()
    hist = list(str(x[0]) for x in query)
    if array<>hist:
        print "ERRORE CHEK DB SAVED DATA: (", user,",",   sect,",", key,")"
        print "Test: ", msg
        print "Atteso  :", array
        print "presente:", hist
        sys.exit(1)
    print "OK CHEK DB SAVED DATA: (", user,",",   sect,",", key,")"
    print "Test : ", msg
    print "ktype: ", ktype
    print "hlen : ", hlen
    print "Dati : ", hist
    print
    

if __name__ == '__main__':
    dbname="prova.db"
    user=4
    section=3
    conc=5
    resetdb=True
    threadmode=False
    history_len=10
    key_for_form=10
    insertedValueForkey=1000
    largetest=True
    hidb=openHistoryDb(dbname)
    test_UNIQUELIST_And_HLEN=True
    
    if resetdb:
        initDB(hidb)
    
    if test_UNIQUELIST_And_HLEN:
        
        #Verifica il lancio di un eccezione se si inizializza la classe con un nome parametro errato
        testWrong_nameKey="NO EXCEPTION"
        try:
            utest = storeHistory(hidb, "TestLISTtoUNIQUELIST", "S1", Wrong_name_key="PIPPO")
        except prgError, e:
            msg=str(e)
            if "Errore: parametro sconosciuto" in msg:
                testWrong_nameKey="PASS"
                print "PASS eccezione prgError per Errore Wrong_name_key=\"PIPPO\"", " -- error msg = %s"%msg
            else:
                print "FAIL eccezione prgError per Errore Wrong_name_key=\"PIPPO\"", " -- error msg = %s"%msg
                sys.exit(1)
        if testWrong_nameKey<>"PASS":
            print 'Errore Wrong_name_key="PIPPO" non ha generato l\'eccezione prevista'
            sys.exit(1)
        
            
        #Test Eccezione su settaggio errato parametro defa_ktype="PIPPO"
        testWrong_defa_ktype="NO EXCEPTION"
        try:
            utest = storeHistory(hidb, "TestLISTtoUNIQUELIST", "S1", defa_ktype="PIPPO") #defa_hlen, dafa_kyype defa_hord
        except prgError, e:
            msg=str(e)
            if "Errore: Parametro defa_ktype" in msg:
                testWrong_defa_ktype="PASS"
                print "PASS eccezione prgError per Errore defa_ktype=\"PIPPO\"", " -- error msg = %s"%msg
            else:
                print "FAIL eccezione prgError per Errore defa_ktype=\"PIPPO\"", " -- error msg = %s"%msg
                sys.exit(1)
        if testWrong_defa_ktype<>"PASS":
            print 'Errore defa_ktype="PIPPO" non ha generato l\'eccezione prevista'
            sys.exit(1)
            
        #Test Eccezione su settaggio errato parametro defa_hord="PIPPO"
        testWrong_defa_hord="NO EXCEPTION"
        try:
            utest = storeHistory(hidb, "TestLISTtoUNIQUELIST", "S1", defa_hord="PIPPO") #defa_hlen, dafa_kyype defa_hord
        except prgError, e:
            msg=str(e)
            if "Errore: Parametro defa_hord" in msg:
                testWrong_defa_hord="PASS"
                print "PASS eccezione prgError per Errore defa_hord=\"PIPPO\"", " -- error msg = %s"%msg
            else:
                print "FAIL eccezione prgError per Errore defa_hord=\"PIPPO\"", " -- error msg = %s"%msg
                sys.exit(1)
        if testWrong_defa_hord<>"PASS":
            print 'Errore defa_hord="PIPPO" non ha generato l\'eccezione prevista'
            sys.exit(1)
       
        Utest = storeHistory(hidb, "TestLISTtoUNIQUELIST", "S1", defa_hlen=10, defa_ktype="LIST")# Nota 10 e "LIST" sono solo un default per la creazione di nuove chiavi. Se il database contiene già delle chiavi con valori diverse non saranno utilizzati
        
        #Verifica il lancio di un eccezione se tenta di fare l'update di una chiave con un nome errato nei parametri
        testWrong_nameKey="NO EXCEPTION"
        try:
            Utest.update("k1", "value 1", Wrong_name_key="PIPPO", hlen=10)
        except prgError, e:
            msg=str(e)
            if "Errore: parametro sconosciuto" in msg:
                testWrong_nameKey="PASS"
                print "PASS eccezione prgError per Errore Wrong_name_key=\"PIPPO\"", " -- error msg = %s"%msg
            else:
                print "FAIL eccezione prgError per Errore Wrong_name_key=\"PIPPO\"", " -- error msg = %s"%msg
                sys.exit(1)
        if testWrong_nameKey<>"PASS":
            print 'Errore Wrong_name_key="PIPPO" non ha generato l\'eccezione prevista'
            sys.exit(1)
           
        #Test Eccezione su settaggio errato parametro ktype="PIPPO"
        testWrong_ktype="NO EXCEPTION"
        try:
            Utest.update("k1", "value 1", ktype="PIPPO", hlen=10)
        except prgError, e:
            msg=str(e)
            if "Errore: Parametro ktype" in msg:
                testWrong_ktype="PASS"
                print "PASS eccezione prgError per Errore ktype=\"PIPPO\"", " -- error msg = %s"%msg
            else:
                print "FAIL eccezione prgError per Errore ktype=\"PIPPO\"", " -- error msg = %s"%msg
                sys.exit(1)
        if testWrong_ktype<>"PASS":
            print 'Errore ktype="PIPPO" non ha generato l\'eccezione prevista'
            sys.exit(1)    
        
        
        Utest.update("k1", "value 1", ktype="LIST", hlen=10) #Indipendentemente da come era prima riporta la coda a paramentri noti
        Utest.update("k1", "value 2")
        Utest.update("k1", "value 2")
        Utest.update("k1", "value 3")
        Utest.update("k1", "value 3")
        Utest.update("k1", "value 3")
        Utest.update("k1", "value 4")
        Utest.update("k1", "value 4")
        Utest.update("k1", "value 4")
        Utest.update("k1", "value 4") #Qua la coda fifo contiene questi dieci elementi... Qualsiasi elemento precedente è uscito dalla coda
        #Utest.debugPrintSection('k1')
        (last, hist)=Utest.HistoryLoadKey("k1")
        print hist
        # MI ASPETTO: ('value 4', ['value 4', 'value 4', 'value 4', 'value 4', 'value 3', 'value 3', 'value 3', 'value 2', 'value 2', 'value 1'])
        check_loadHistory("Inserimento valori su una LIST che consente i doppioni", 
            (last, hist), 
            ('value 4', ['value 4', 'value 4', 'value 4', 'value 4', 'value 3', 'value 3', 'value 3', 'value 2', 'value 2', 'value 1'])
        )
        Utest.update("k1", "value 1", ktype="UNIQUELIST")
        #Utest.debugPrintSection('k1')
        (last, hist)=Utest.HistoryLoadKey("k1")
        # Mi aspetto che siano stati tolti tutti i valori doppi e che "value 1" che ho inserito e risulta doppio
        # sia stato spostato come ultimo elemento (elemento 0 visto che li richiamo in ordine inverso)
        # MI ASPETTO: ('value 1', ['value 1', 'value 4', 'value 3', 'value 2'])
        check_loadHistory("Trasformazione in una lista UNIQUELIST che consente i doppioni", 
            (last, hist), 
            ('value 1', ['value 1', 'value 4', 'value 3', 'value 2'] )
        )
        Utest.update("k1", "value 5")
        Utest.update("k1", "value 6")
        Utest.update("k1", "value 7")
        Utest.update("k1", "value 8")
        Utest.update("k1", "value 9")
        Utest.update("k1", "value 10")
        Utest.update("k1", "value 11") #Inseriti altri sette elementi (per un totale di 11) il primo elemento della lista deve venire tagliato.
        #Utest.debugPrintSection('k1')
        (last, hist)=Utest.HistoryLoadKey("k1")
        # Ho inserito un totale di 11 elementi in una lista da 10 mi aspetto che il primo 
        # "value 2" (l'ultimo sull'array che è rovesciato) sia uscito dalla coda
        #('value 11', ['value 11','value 10','value 9','value 8','value 7','value 6','value 5','value 1', 'value 4', 'value 3'] )
        check_loadHistory("USCITA dell ELEMENTO value 2 dalla Lista di 10 elementi", 
            (last, hist), 
            ('value 11', ['value 11','value 10','value 9','value 8','value 7','value 6','value 5','value 1', 'value 4', 'value 3'] )
        )
        Utest.update("k1", "value 12", hlen=5) #Aggiungo un elemento e taglio la lista a 5 ELEMENTI
        #Utest.debugPrintSection('k1')
        (last, hist)=Utest.HistoryLoadKey("k1")
        # Lista tagliata a soli 5 elementi dopo l'aggiunta di value 12.
        # Mi aspetto che tutti gli elemente più vecchi degli ultimi 5 inserimenti siano stati eliminati.
        # ('value 12', ['value 12','value 11','value 10','value 9','value 8'] )
        check_loadHistory("Lista TAGLIATA a 5 Elementi", 
            (last, hist), 
            ('value 12', ['value 12','value 11','value 10','value 9','value 8'] )
        )
        
        Utest.update("k1", "value 12") # DOPPIO portato in cima (ma lo era già)
        Utest.update("k1", "value 13") # SESTO elemento di una lista di 5 Espelle value 8
        Utest.update("k1", "value 13") # DOPPIO portato in cima (ma lo era già)
        #Utest.debugPrintSection('k1')
        #Mi aspetto una lista di 5 elementi in cui value 8 sia stato espulso.
        #('value 13', ['value 13','value 12','value 11','value 10','value 9'] )
        (last, hist)=Utest.HistoryLoadKey("k1")
        check_loadHistory("Controllo che non ci siano inserimenti doppi e \n Controllo espulsione di value 8 perchè sesto elemento di una lista di 5", 
            (last, hist), 
            ('value 13', ['value 13','value 12','value 11','value 10','value 9'] )
        )
        Utest.update("k1", "value 13", ktype="LIST", hlen=10) #!!! Ora i doppioni sono di nuovo ammessi. questo value 13 sarà inserito e la lista viene riportata a 10 elementi
        Utest.update("k1", "value 14")
        Utest.update("k1", "value 14")
        Utest.update("k1", "value 15")
        Utest.update("k1", "value 15")
        Utest.update("k1", "value 16 Espelle value 9") #Questo è l'undicesiomo elemento di una lista di 10 espelle value 9
        #Utest.debugPrintSection('k1')
        #Mi aspetto una lista tornata a 10 Elementi con doppioni in cui value 9 sia stato espulso perchè undicesimo elemnto
        #('value 16 Espelle value 9','value 16 Espelle value 9', 'value 15','value 15','value 14','value 14', 'value 13','value 13','value 12','value 11','value 10'])
        (last, hist)=Utest.HistoryLoadKey("k1")
        check_loadHistory("Controllo che non ci siano inserimenti doppi e \n Controllo espulsione di value 8 perchè sesto elemento di una lista di 5", 
            (last, hist), 
            ('value 16 Espelle value 9',['value 16 Espelle value 9', 'value 15','value 15','value 14','value 14', 'value 13','value 13','value 12','value 11','value 10'])
        )
        Utest.update("k2", "h value 0", ktype="UNIQUELIST", hlen=5) #Nuova Chiave K2 Indipendentemente da come era prima riporta la coda a paramentri noti. Eventuali valori vecchi verranno espulsi
        Utest.update("k2", "f value 1")
        Utest.update("k2", "b value 2")
        Utest.update("k2", "c value 3")
        Utest.update("k2", "i value 4")
        Utest.update("k2", "l value 5") #Espelle h value 0
        Utest.update("k2", "d value 6") #espelle f value 1
        Utest.update("k2", "a value 7") #espelle b value 2
        Utest.update("k2", "e value 8") #espelle c value 3
        Utest.update("k2", "g value 9") #espelle i value 4
        #Utest.debugPrintSection('k2')
        (last, hist)=Utest.HistoryLoadKey("k2")
        #Mi aspetto che sulla nuova chiave K2 siano presenti solo gli ultimi 5 elementi che ho inserito
        #('g value 9',['g value 9', 'e value 8','a value 7','d value 6','l value 5'])
        check_loadHistory("Nuova chiave k2 Gli ultimi 5 elementi inseriti", 
            (last, hist), 
            ('g value 9',['g value 9', 'e value 8','a value 7','d value 6','l value 5'])
        )
        (last, hist)=Utest.HistoryLoadKey("k2", "RevIns")
        # RevIns : è gia l'ordine di default. Mi spetto lo stesso risultato del check precedente
        check_loadHistory("ordine RevIns è quello di default deve tornare la stessa cosa del test precedente", 
            (last, hist), 
            ('g value 9',['g value 9', 'e value 8','a value 7','d value 6','l value 5'])
        )
        (last, hist)=Utest.HistoryLoadKey("k2", "Ins")
        # Ins :Ordine di inserimento inverso. mi aspetto l'array di prima capovolto. Nota last rimane sempre invariato.
        check_loadHistory("ordine Ins L'ultimo elemento sull'array sarà l'ultimo inserito (RevIns capovolto) - Last rimane sempre l'ultimo inserito", 
            (last, hist), 
            ('g value 9',['l value 5','d value 6','a value 7','e value 8','g value 9'])
        )
        (last, hist)=Utest.HistoryLoadKey("k2", "Alfa")
        # Alfa : Ordine alfabetico
        check_loadHistory("ordine Alfa = ordine alfabetico - Last rimane sempre l'ultimo inserito", 
            (last, hist), 
            ('g value 9',['a value 7','d value 6', 'e value 8','g value 9', 'l value 5'])
        )
        (last, hist)=Utest.HistoryLoadKey("k2", "RevAlfa")
        # RevAlfa : Ordine alfabetico INVERSO
        check_loadHistory("ordine Alfa = ordine alfabetico - Last rimane sempre l'ultimo inserito", 
            (last, hist), 
            ('g value 9',['l value 5', 'g value 9', 'e value 8', 'd value 6', 'a value 7'])
        )
        
        Utest.update("k3", "value 1", hlen=10,ktype="UNIQUELIST") #Verifica che ktype="UNIQUELIST" sovrascriva il default "LIST" impostato in creazione oggetto history. Nota lo verifica la funzione che fa il check dei dati scritti sul DB
        Utest.update("k3", "value 2")
        Utest.update("k3", "value 3")
        Utest.update("k3", "value 4")
        Utest.update("k3", "value 5")
        Utest.update("k3", "value 6")
        Utest.update("k3", "value 7")
        Utest.update("k3", "value 8")
        Utest.update("k3", "value 9")
        Utest.update("k3", "value 10") #Creo/apro una lista 3 di 10 Elementi (Eventuali elementi precedenti saranno stati espulsi)
        # non la uso subito, la riutilizzero dopo aver chiuso e riaperto la maschera per vedere che abbia salvato correttamente i dati sul cursore.
        Utest.close()
        #VERIFICA CORRETTO SALAVAGGIO DATI SUL DB
        checkRevDB(dbname, "k1 Ultima posizione nota", "TestLISTtoUNIQUELIST","S1", "k1", ['value 16 Espelle value 9', 'value 15','value 15','value 14','value 14', 'value 13','value 13','value 12','value 11','value 10'], "LIST", 10)
        checkRevDB(dbname, "k2 Ultima posizione nota", "TestLISTtoUNIQUELIST","S1", "k2", ['g value 9', 'e value 8','a value 7','d value 6','l value 5'] ,"UNIQUELIST",5 )
        checkRevDB(dbname, "k3 Ultima posizione nota", "TestLISTtoUNIQUELIST","S1", "k3", ['value 10', 'value 9','value 8','value 7','value 6', 'value 5','value 4','value 3','value 2','value 1'], "UNIQUELIST",10)
        Utest = storeHistory(hidb, "TestLISTtoUNIQUELIST", "S1")

        # Riapro L'history e verifico che abbia creato correttamente il cursore per le 3 chiavi
        (last, hist)=Utest.HistoryLoadKey("k1")
        check_loadHistory("Riaperture history, verifica ultima chiave k1 nota", 
            (last, hist), 
            ('value 16 Espelle value 9',['value 16 Espelle value 9', 'value 15','value 15','value 14','value 14', 'value 13','value 13','value 12','value 11','value 10'])
        )
        (last, hist)=Utest.HistoryLoadKey("k2")
        check_loadHistory("Riaperture history, verifica ultima chiave k2 nota", 
            (last, hist), 
            ('g value 9',['g value 9', 'e value 8','a value 7','d value 6','l value 5'])
        )
        (last, hist)=Utest.HistoryLoadKey("k3")
        check_loadHistory("Riaperture history, verifica ultima chiave k3 nota", 
            (last, hist), 
            ('value 10',['value 10', 'value 9','value 8','value 7','value 6', 'value 5','value 4','value 3','value 2','value 1'])
        )
        Utest.createNewKeyIfNotExistAndGetID("k3", hlen=5)#Taglio k3 a 5 Valori
        Utest.close() #e chiudo nuovamente l'history per verificare che in fase di update del DB vengano effettivamente eliminate le chiavi in eccesso.
        #VERIFICA CORRETTO SALAVAGGIO DATI SUL DB
        checkRevDB(dbname, "k1 Ultima posizione nota Dopo taglio k3", "TestLISTtoUNIQUELIST","S1", "k1", ['value 16 Espelle value 9', 'value 15','value 15','value 14','value 14', 'value 13','value 13','value 12','value 11','value 10'], "LIST",10)
        checkRevDB(dbname, "k2 Ultima posizione nota Dopo taglio k3", "TestLISTtoUNIQUELIST","S1", "k2", ['g value 9', 'e value 8','a value 7','d value 6','l value 5'], "UNIQUELIST",5)
        checkRevDB(dbname, "k3 Tagliata a 5 elementi", "TestLISTtoUNIQUELIST","S1", "k3", ['value 10', 'value 9','value 8','value 7','value 6'], "UNIQUELIST",5)
        Utest = storeHistory(hidb, "TestLISTtoUNIQUELIST", "S1") #Riapro 
        #e verifico che su k3 siano rimasti solo 5 elementi.
        (last, hist)=Utest.HistoryLoadKey("k1")
        check_loadHistory("Riaperture history dopo taglio k3, verifica ultima chiave k1 nota", 
            (last, hist), 
            ('value 16 Espelle value 9',['value 16 Espelle value 9', 'value 15','value 15','value 14','value 14', 'value 13','value 13','value 12','value 11','value 10'])
        )
        (last, hist)=Utest.HistoryLoadKey("k2")
        check_loadHistory("Riaperture history dopo taglio k3, verifica ultima chiave k2 nota", 
            (last, hist), 
            ('g value 9',['g value 9', 'e value 8','a value 7','d value 6','l value 5'])
        )
        (last, hist)=Utest.HistoryLoadKey("k3")
        check_loadHistory("Riaperture history dopo taglio k3, verifica che chiave k3 contenga 5 elemnti", 
            (last, hist), 
            ('value 10',['value 10', 'value 9','value 8','value 7','value 6'])
        )
        Utest.createNewKeyIfNotExistAndGetID("k3", hlen=3)# Un nuovo taglio a K3 Utilizzo la func createNewKeyIfNotExistAndGetID  che ancora non avevo usato direttamente (mi permette di tagliare senza inserire elemnti nuovi)
        Utest.updateDB() # del Db Senza chiudere l'histpory. Dopo l'update il cursore viene riletto
        #VERIFICA CORRETTO SALAVAGGIO DATI SUL DB e taglio di k3 a 3 elementi
        checkRevDB(dbname, "k1 Ultima posizione nota Dopo taglio k3 a 3 elemnti (check Func updateDB)", "TestLISTtoUNIQUELIST","S1", "k1", ['value 16 Espelle value 9', 'value 15','value 15','value 14','value 14', 'value 13','value 13','value 12','value 11','value 10'], "LIST",10)
        checkRevDB(dbname, "k2 Ultima posizione nota Dopo taglio k3 a 3 elemnti (check Func updateDB)", "TestLISTtoUNIQUELIST","S1", "k2", ['g value 9', 'e value 8','a value 7','d value 6','l value 5'], "UNIQUELIST",5)
        checkRevDB(dbname, "k3 Tagliata a 3 elementi (check Func updateDB)", "TestLISTtoUNIQUELIST","S1", "k3", ['value 10', 'value 9','value 8'], "UNIQUELIST",3)
        #Verifico rilettura cursore e che su k3 siano rimasti solo 3 elementi.
        (last, hist)=Utest.HistoryLoadKey("k1")
        check_loadHistory("verifica cursore history dopo taglio k3 a 3 elemnti (check Func updateDB), verifica ultima chiave k1 nota", 
            (last, hist), 
            ('value 16 Espelle value 9',['value 16 Espelle value 9', 'value 15','value 15','value 14','value 14', 'value 13','value 13','value 12','value 11','value 10'])
        )
        (last, hist)=Utest.HistoryLoadKey("k2")
        check_loadHistory("verifica cursore history dopo taglio k3 a 3 elemnti (check Func updateDB), verifica ultima chiave k2 nota", 
            (last, hist), 
            ('g value 9',['g value 9', 'e value 8','a value 7','d value 6','l value 5'])
        )
        (last, hist)=Utest.HistoryLoadKey("k3")
        check_loadHistory("verifica cursore history dopo taglio k3 a 3 elemnti (check Func updateDB), verifica che chiave k3 contenga 3 elemnti", 
            (last, hist), 
            ('value 10',['value 10', 'value 9','value 8'])
        )
        Utest.createNewKeyIfNotExistAndGetID("k3", hlen=1) #Taglio di K3 ad un solo elemento
        (last, hist)=Utest.HistoryLoadKey("k3")
        check_loadHistory("Verifica taglio k3 ad un solo elemento", 
            (last, hist), 
            ('value 10',['value 10'])
        )
        Utest.createNewKeyIfNotExistAndGetID("k3", hlen=0)# nessuno errore Toglie tutte le righe e imposta hlen=0
        (last, hist)=Utest.HistoryLoadKey("k3")
        check_loadHistory("Verifica taglio k3 ZERO Elementi Ritorna una tupla con una stringa vuota e un array vuoto senza che si verifichino errori", 
            (last, hist), 
            ('',[''])
        )
        Utest.createNewKeyIfNotExistAndGetID("k3", hlen=-1) # nessuno errore Toglie tutte le righe e imposta hlen=0 (NON -1)
        (last, hist)=Utest.HistoryLoadKey("k3")
        check_loadHistory("Verifica taglio k3 -1 Elementi I numeri negativi vengono trasformati in ZERO e quindi ottengo lo stesso effetto del test sopra, senza che si verifichino errori", 
            (last, hist), 
            ('',[])
        )
        Utest.createNewKeyIfNotExistAndGetID("k4", hlen=7) #Creazione di una chiave vuota per verifica comportamento alla riapertura. E' vuota ma se Quando farò il check della lunghezza sul db potrà contenere 7 ELEMENTI. Su questa apertura non ho impostato un tipo per ktype e Quindi prende il DEFAULT Uniquelist
        (last, hist)=Utest.HistoryLoadKey("k4")
        check_loadHistory("La lista k4 è stata creata con solo la testata questa test deve restituire una tupla con una stringa vuota e un array vuoto , senza che si verifichino errori", 
            (last, hist), 
            ('',[])
        )
        Utest.createNewKeyIfNotExistAndGetID("k5") #Creazione di una chiave vuota per verifica comportamento alla riapertura. E' vuota ma se Quando farò il check della lunghezza sul db potrà contenere 7 ELEMENTI. Su questa apertura non ho impostato un tipo per ktype e Quindi prende il DEFAULT Uniquelist
        (last, hist)=Utest.HistoryLoadKey("k5")
        check_loadHistory("La lista k5 è stata creata con solo la testata e i valori di default hlen=50, ktype=\"UNIQUELIST\" questa test deve restituire una tupla con una stringa vuota e un array vuoto , senza che si verifichino errori", 
            (last, hist), 
            ('',[])
        )
        Utest.close()
        #Chiudo e verifico i salvataggi
        checkRevDB(dbname, "k1 Ultima posizione nota Dopo taglio k3 a 0 elementi e aggiunta k4 e k5 vuote (check Func updateDB)", "TestLISTtoUNIQUELIST","S1", "k1", ['value 16 Espelle value 9', 'value 15','value 15','value 14','value 14', 'value 13','value 13','value 12','value 11','value 10'], "LIST",10)
        checkRevDB(dbname, "k2 Ultima posizione nota Dopo taglio k3 a 0 elementi e aggiunta k4 e k5 vuote (check Func updateDB)", "TestLISTtoUNIQUELIST","S1", "k2", ['g value 9', 'e value 8','a value 7','d value 6','l value 5'],"UNIQUELIST",5)
        checkRevDB(dbname, "k3 Vuota", "TestLISTtoUNIQUELIST","S1", "k3", [], "UNIQUELIST",0)
        checkRevDB(dbname, "k4 Vuota hlen 7", "TestLISTtoUNIQUELIST","S1", "k4", [], "UNIQUELIST",7)
        checkRevDB(dbname, "k5 Vuota hlen 50 preso da impostazioni di default", "TestLISTtoUNIQUELIST","S1", "k5", [], "UNIQUELIST",50)
        Utest = storeHistory(hidb, "TestLISTtoUNIQUELIST", "S1")
        #Riapro e verifico comportamento in presenza di liste vuote
        (last, hist)=Utest.HistoryLoadKey("k1")
        check_loadHistory("Riaperture history Con k3 e k4 e k5  vuote, verifica ultima chiave k1 nota", 
            (last, hist), 
            ('value 16 Espelle value 9',['value 16 Espelle value 9', 'value 15','value 15','value 14','value 14', 'value 13','value 13','value 12','value 11','value 10'])
        )
        (last, hist)=Utest.HistoryLoadKey("k2")
        check_loadHistory("Riaperture history Con k3 e k4 e k5  vuote, verifica ultima chiave k2 nota", 
            (last, hist), 
            ('g value 9',['g value 9', 'e value 8','a value 7','d value 6','l value 5'])
        )
        (last, hist)=Utest.HistoryLoadKey("k3")
        check_loadHistory("Riaperture history Con k3 e k4 e k5  vuote, verifica che chiave k3 sia vuota", 
            (last, hist), 
            ('',[])
        )
        (last, hist)=Utest.HistoryLoadKey("k4")
        check_loadHistory("Riaperture history Con k3 e k4 e k5  vuote, verifica che chiave k4 sia vuota", 
            (last, hist), 
            ('',[])
        )
        (last, hist)=Utest.HistoryLoadKey("k5")
        check_loadHistory("Riaperture history Con k3 e k4 e k5 vuote, verifica che chiave k4 sia vuota", 
            (last, hist), 
            ('',[])
        )
        # Test di una chiave non presente
        (last, hist)=Utest.HistoryLoadKey("KEY_NON_PRESENTE")
        check_loadHistory("Richiesta di una key non presente KEY_NON_PRESENTE Torna una tupla fatta da una stringa e un array vuoto senza generare errori", 
            (last, hist), 
            ('',[])
        )
        #Test tentativo di richiedere una key con un ordine che non esiste
        testWrong_order="NO EXCEPTION"
        try:
            (last, hist)=Utest.HistoryLoadKey("k2", "OrdineNonEsistente")
        except prgError, e:
            msg=str(e)
            if "Errore: Parametro ord" in msg:
                testWrong_order="PASS"
                print "PASS eccezione prgError per ordine = OrdineNonEsistente", " -- error msg = %s"%msg
            else:
                print "FAIL eccezione prgError per ordine = OrdineNonEsistente", " -- error msg = %s"%msg
                sys.exit(1)
        if testWrong_order<>"PASS":
            print 'Errore ordine = OrdineNonEsistente non ha generato l\'eccezione prevista'
            sys.exit(1)    
        
        Utest.close()
    
    if largetest:
        n=0    
        tr=[]
        for c in range (1, conc+1):
            for u in range (1, user+1):
                str_user='User %d'%u
                for s in range (1, section+1):
                    str_sect='Section %d'%s
                    str_tread="T %d, Serie %d, User %d Section %d"%(n, c, u, s)
                    tr.append( TestTread(str_tread,hidb, str_user, str_sect, history_len, key_for_form,insertedValueForkey) )
                    n+=1
            
        if threadmode==False:
            for x in tr:
                x.run()
        else:
            for x in tr:
                x.start()
            #Aspetto che terminino
            for x in tr:
                x.join()
                
    print "lista connessioni aperte", hidb.listcon
    print "numero di connessioni aperte", len(hidb.listcon)
    print "Eseguo check che tutte le connessioni siano chiuse"
    hidb.onCloseCheckConnectionLeak()
    print "terminato"
